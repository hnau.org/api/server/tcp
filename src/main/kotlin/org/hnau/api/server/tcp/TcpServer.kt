package org.hnau.api.server.tcp

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import org.hnau.base.data.bytes.provider.BytesProvider
import org.hnau.base.data.bytes.provider.toBytesProvider
import org.hnau.base.data.bytes.receiver.BytesReceiver
import org.hnau.base.data.bytes.receiver.toBytesReceiver
import org.hnau.base.data.time.Time
import org.hnau.base.data.time.asSeconds
import java.lang.Exception
import java.net.ServerSocket
import java.net.SocketException


fun TcpApiServer(
        port: Int,
        handler: suspend BytesProvider.() -> (BytesReceiver.() -> Unit),
        onThrowableWasThrownWhenHandlingClient: (Throwable) -> Unit,
        timeout: Time = 10.asSeconds
): () -> Unit {

    val clientsHandlersJob = SupervisorJob()
    val clientsHandlersContext = Dispatchers.IO + clientsHandlersJob

    val serverSocket = ServerSocket(port)
    GlobalScope.launch(
            context = Dispatchers.IO
    ) {
        var serverSocketIsActive = true
        while (serverSocketIsActive) {
            try {
                val clientSocket = serverSocket.accept()
                GlobalScope.launch(
                        context = clientsHandlersContext
                ) {
                    try {
                        clientSocket.soTimeout = timeout.milliseconds.toInt()
                        val bytesProvider = clientSocket.getInputStream().toBytesProvider()
                        val responseWriter = handler(bytesProvider)
                        clientSocket.getOutputStream().run {
                            toBytesReceiver().responseWriter()
                            flush()
                        }
                    } catch (th: Throwable) {
                        onThrowableWasThrownWhenHandlingClient(th)
                    }
                }
            } catch (ex: SocketException) {
                clientsHandlersJob.cancel()
                serverSocketIsActive = false
            }
        }
    }

    return serverSocket::close
}